# README #

Python3 script for running an info screen on a raspberry pi 1-4

## Install ##

### Raspbian ###

- Install raspbian
	- Raspberry pi 1-3, install desktop
	- Raspberry pi 4, install console

### LCD Drivers ###

- git clone https://github.com/goodtft/LCD-show.git
- chmod -R 755 LCD-show
- cd LCD-show
- sudo ./LCD5-show # This will install drivers and restart
- Make sure, if rpi4, that you are connected to hdmi0, not 1

### Python3 and Kivy library ###

- sudo apt update && sudo apt install python3-setuptools git-core python3-dev

### Source installation Dependencies ###

- sudo apt install pkg-config libgl1-mesa-dev libgles2-mesa-dev \
	libgstreamer1.0-dev \
	gstreamer1.0-plugins-{bad,base,good,ugly} \
	gstreamer1.0-{omx,alsa} libmtdev-dev \
	xclip xsel libjpeg-dev

### DESKTOP Raspberry Pi 1-4 Desktop environment ###
### If you have installed Raspbian with a desktop i.e. if your Raspberry Pi boots into a desktop environment then install SDL2 from apt: ###

- sudo apt install libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev
- sudo adduser pi render # Maybe?

### CONSOLE ###
### Raspberry Pi 4 headless installation on Raspbian Buster ###
### If you run Kivy from the console and not from a desktop environment, you need to compile SDL2 from source, as the one bundled with Buster is not compiled with the kmsdrm backend, so it only works under X11. ###

- sudo apt-get install libfreetype6-dev libgl1-mesa-dev libgles2-mesa-dev libdrm-dev libgbm-dev libudev-dev libasound2-dev liblzma-dev libjpeg-dev libtiff-dev libwebp-dev git build-essential
- sudo apt-get install gir1.2-ibus-1.0 libdbus-1-dev libegl1-mesa-dev libibus-1.0-5 libibus-1.0-dev libice-dev libsm-dev libsndio-dev libwayland-bin libwayland-dev libxi-dev libxinerama-dev libxkbcommon-dev libxrandr-dev libxss-dev libxt-dev libxv-dev x11proto-randr-dev x11proto-scrnsaver-dev x11proto-video-dev x11proto-xinerama-dev

### Install SDL2: ###

- wget https://libsdl.org/release/SDL2-2.0.10.tar.gz
- tar -zxvf SDL2-2.0.10.tar.gz
- pushd SDL2-2.0.10
- ./configure --enable-video-kmsdrm --disable-video-opengl --disable-video-x11 --disable-video-rpi
- make -j$(nproc)
- sudo make install
- popd

### Install SDL2_image: ###

- wget https://libsdl.org/projects/SDL_image/release/SDL2_image-2.0.5.tar.gz
- tar -zxvf SDL2_image-2.0.5.tar.gz
- pushd SDL2_image-2.0.5
- ./configure
- make -j$(nproc)
- sudo make install
- popd

### Install SDL2_mixer: ###

- wget https://libsdl.org/projects/SDL_mixer/release/SDL2_mixer-2.0.4.tar.gz
- tar -zxvf SDL2_mixer-2.0.4.tar.gz
- pushd SDL2_mixer-2.0.4
- ./configure
- make -j$(nproc)
- sudo make install
- popd

### Install SDL2_ttf: ###

- wget https://libsdl.org/projects/SDL_ttf/release/SDL2_ttf-2.0.15.tar.gz
- tar -zxvf SDL2_ttf-2.0.15.tar.gz
- pushd SDL2_ttf-2.0.15
- ./configure
- make -j$(nproc)
- sudo make install
- popd
- sudo adduser pi render

### Install kivy ###

- sudo apt install python3-pip
- python3 -m pip install --upgrade pip setuptools virtualenv
- python3 -m pip install kivy[full] kivy_examples

### Hide cursor, run once python3, then import kivy to created config file ###

- python3
- import kivy
- exit()
- vim /home/pi/.kivy/config.ini

### change show_cursor to ###

- show_cursor = 0

### Project dependencies ###

- python3 -m pip install adafruit-circuitpython-mcp9808

### graphql ###

- python3 -m pip install gql

### Peewee: ###

- python3 -m pip install peewee

### To test connection ###

- sudo apt install mariadb-client
- python3 -m pip install PyMySql


### Usb turned off to save power ###

- echo '1-1' |sudo tee /sys/bus/usb/drivers/usb/unbind

### Turn on again ###

- echo '1-1' |sudo tee /sys/bus/usb/drivers/usb/bind

### CPU throttled /boot/config.txt by adding the following ###

- arm_freq_min=250
- core_freq_min=100
- sdram_freq_min=150
- over_voltage_min=0

### Bluetooth disabled in /boot/config.txt by adding the following ###

- dtoverlay=pi3-disable-bt

### Leds disabled in /boot/config.txt ###

- dtparam=act_led_trigger=none
- dtparam=act_led_activelow=on

### Autostart ###

- crontab -e
- @reboot sleep 30 && /home/pi/uninfopy3/uninfopy3.py
